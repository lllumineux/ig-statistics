# IG Statistics

## What's this app?
This is a console-based application for analyzing Instagram accounts and determining the companies that the user has advertised the most.

At startup, a list of accounts for analysis is specified, as well as a number of settings (for more details, see the "Arguments" and "Options" sections below). First, the application parses the account data. It then sequentially parses and saves the posts, along with the determining which of the accounts mentioned in the post are NOT personal. After that, it generates an HTML-file with an interactive chart that shows names of companies sorted by the frequency of mentions.

## How to run?

### Preparing
1. `pip3 install poetry`
2. `poetry install`
3. `cp .env.example .env`
4. `python3 src/db/create.py`

### Running

#### Schema
`python3 src/main.py [ARGUMENTS] [OPTIONS]`

#### Example
`python3 src/main.py @account1 @account2 --no-update --min-year=2018`

## Arguments

### accounts
List of accounts to process.

## Options

### --min-year
Posts published earlier than the specified year will not be processed. *Default: 2017*.

### --min-followers
Accounts with less than the specified number of followers will not be displayed. *Default: 5000*.

### --update / --no-update
Either fetch data or use the saved one. *Default: --update*.

### --output-directory
Name of the directory where to store graphs. *Default: "stats"*.

### --help
Display available arguments and options.
