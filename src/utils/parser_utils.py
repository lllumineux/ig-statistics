import os
import time
from datetime import datetime
from typing import Union

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from initialization import driver
from utils.utils import prettify_text, extended_print


def start() -> None:
    extended_print('Logging in', msg_effect='wait')

    driver.get('https://www.instagram.com/')
    time.sleep(4)

    username_field = driver.find_element(By.CSS_SELECTOR, 'input[name="username"]')
    username_field.clear()
    username_field.send_keys(os.getenv('INSTAGRAM_LOGIN'))
    time.sleep(2)

    password_field = driver.find_element(By.CSS_SELECTOR, 'input[name="password"]')
    password_field.clear()
    password_field.send_keys(os.getenv('INSTAGRAM_PASSWORD'))
    time.sleep(2)

    login_btn = driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]')
    login_btn.click()
    time.sleep(4)

    extended_print('Successfully logged in', msg_effect='success', remove_prev_line=True)


def get_account(username: str) -> Union[dict, None]:
    extended_print(f'Parsing @{username} account info', msg_effect='wait')

    driver.execute_script('window.open("");')
    time.sleep(1)
    driver.switch_to.window(driver.window_handles[-1])
    time.sleep(1)
    driver.get(f'https://www.instagram.com/{username}')
    time.sleep(4)

    try:
        name_el = driver.find_element(By.XPATH, '//section//ul/following-sibling::div/span')
        name = prettify_text(name_el.get_attribute('innerHTML'))
    except NoSuchElementException:
        name = None

    try:
        category_el = driver.find_element(
            By.XPATH,
            '//section//ul/following-sibling::div/span/following-sibling::div//div'
        )
        category = prettify_text(category_el.get_attribute('innerHTML'))
    except NoSuchElementException:
        try:
            category_el = driver.find_element(By.XPATH, '//section//ul/following-sibling::div/div/div')
            category = prettify_text(category_el.get_attribute('innerHTML'))
        except NoSuchElementException:
            category = None

    try:
        followers_number_el = driver.find_element(
            By.XPATH,
            '(//section//ul//div)[contains(text(), "followers")]//span'
        )
        followers_number = int(followers_number_el.get_attribute('title').replace(',', ''))
    except NoSuchElementException:
        followers_number = 0

    driver.close()
    time.sleep(1)
    driver.switch_to.window(driver.window_handles[-1])
    time.sleep(1)

    if not name and not followers_number:
        extended_print(f'Error: @{username} is unavailable', msg_effect='error', remove_prev_line=True)
        return None

    extended_print(f'Successfully parsed @{username} account info', msg_effect='success', remove_prev_line=True)

    return {'username': f'@{username}', 'name': name, 'category': category, 'followers_number': followers_number}


def read_post_info(counter) -> Union[dict, None]:
    extended_print(f'Parsing posts: {counter}', msg_effect='wait', remove_prev_line=True)

    try:
        description_el = driver.find_element(By.XPATH, '//article//ul[1]//li[1]//h2/../div/span')
        description = prettify_text(description_el.get_attribute('innerHTML'))
    except NoSuchElementException:
        description = None

    posted_time_el = driver.find_element(By.XPATH, '(//article//time)[last()]')
    posted_time = datetime.strptime(posted_time_el.get_attribute('datetime')[:-5], '%Y-%m-%dT%H:%M:%S')

    return {'description': description, 'posted_time': posted_time}


def get_posts(username: str, min_year: int = 2017) -> dict:
    extended_print('Parsing posts', msg_effect='wait')

    driver.get(f'https://www.instagram.com/{username}')
    time.sleep(4)

    first_post_link = driver.find_element(By.XPATH, '(//article//a)[1]')
    first_post_link.click()
    time.sleep(4)

    counter = 1
    while True:
        post = read_post_info(counter)

        if not post or post['posted_time'].year < int(min_year):
            break

        yield post

        try:
            next_btn = driver.find_element(
                By.XPATH,
                '(//*)[name()="svg"][contains(@aria-label, "Next")]/../../../../button'
            )
            next_btn.click()
            time.sleep(4)
        except NoSuchElementException:
            break

        counter += 1

    extended_print('Successfully parsed posts', msg_effect='success', remove_prev_line=True)


def end() -> None:
    driver.quit()
