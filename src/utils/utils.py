import os
import re
import sys
from datetime import datetime

import translators
from plotly import offline
from plotly.express import bar
from pymystem3 import Mystem

from config import DEFAULT_OUTPUT_DIRECTORY_NAME, EFFECTS, DEFAULT_CATEGORIES_TO_EXCLUDE, DEFAULT_ACCOUNTS_TO_EXCLUDE


def get_mentions_from_description(text: str) -> list:
    mentions = []
    mention_pairs = re.findall(r'(^@[.\w]+)|(\W@[.\w]+)', text)
    for pair in mention_pairs:
        mention = pair[0] if pair[0] else pair[1]
        mention = mention[1:] if mention[0] != '@' else mention
        mention = mention[:-1] if mention[-1] in '.!?' else mention
        mentions.append(mention)
    return mentions


def exclude_personal_accounts(accounts: list, min_followers_number: int = 5000) -> list:
    extended_print('Processing mentions', msg_effect='wait')
    processed_accounts = []
    accounts_number = len(accounts)
    for i in range(accounts_number):
        extended_print(f'Processing mentions: {i + 1}/{accounts_number}', msg_effect='wait', remove_prev_line=True)
        account = accounts[i]
        if all(
            [
                account.username not in DEFAULT_ACCOUNTS_TO_EXCLUDE,
                account.name,
                account.category not in DEFAULT_CATEGORIES_TO_EXCLUDE,
                account.followers_number >= int(min_followers_number)
            ]
        ):
            russian_account_name = translators.google(account.name, from_language='en', to_language='ru')
            is_name_checkers = []
            for word in Mystem().analyze(russian_account_name):
                try:
                    analysis = word['analysis'][0]
                    is_name_checkers.append(
                        any(['имя' in analysis['gr'], 'фам' in analysis['gr']])
                        and analysis['wt'] > 0.75
                    )
                except (IndexError, KeyError):
                    pass
            if not any(is_name_checkers):
                processed_accounts.append(account)
    extended_print('Successfully processed mentions', msg_effect='success', remove_prev_line=True)
    return processed_accounts


def prettify_text(text: str) -> str:
    return text.replace('&amp;', '&')


def generate_graph(
    mentions: list,
    target_account_username: str,
    output_directory_name: str = DEFAULT_OUTPUT_DIRECTORY_NAME
) -> None:
    extended_print('Generating a graph', msg_effect='wait')

    current_date = str(datetime.now().date())
    origin_path = os.getcwd()
    os.chdir('..')
    if not os.path.exists(output_directory_name):
        os.mkdir(output_directory_name)
    os.chdir(output_directory_name)
    if not os.path.exists(current_date):
        os.mkdir(current_date)
    os.chdir(current_date)

    data = []
    unique_mentions = list(set(mentions))
    for mention in unique_mentions:
        data.append({'Mention': mention, 'Count': mentions.count(mention)})

    x_axes = {'dtick': '1'}
    y_axes = {'categoryorder': 'total ascending'}

    fig = bar(
        data_frame=data,
        x='Count',
        y='Mention',
        title=f'{current_date}: <a href="https://www.instagram.com/{target_account_username[1:]}/">'
              f'{target_account_username}'
              f'</a>',
        orientation='h'
    ).update_xaxes(**x_axes).update_yaxes(**y_axes)
    file_name = f'{target_account_username[1:]}.html'
    offline.plot(fig, filename=file_name, auto_open=False)
    extended_print(
        f'Successfully generated a graph: {os.getcwd()}/{file_name}',
        msg_effect='success',
        remove_prev_line=True
    )

    os.chdir(origin_path)


def extended_print(msg, msg_effect: str = None, remove_prev_line: bool = False, **kwargs) -> None:
    if remove_prev_line:
        sys.stdout.write('\033[F')
        sys.stdout.write('\033[K')

    if msg_effect:
        print(f'{EFFECTS[msg_effect]}{msg}{EFFECTS["reset"]}', **kwargs)
    else:
        print(msg, **kwargs)
