from datetime import datetime
from typing import Union

from sqlalchemy.exc import IntegrityError, PendingRollbackError

from db.models import Account, Post, Mention
from initialization import session


# Author


def add_account(username: str, name: str, category: str, followers_number: int) -> Account:
    try:
        account = Account(username=username, name=name, category=category, followers_number=followers_number)
        session.add(account)
        session.commit()
        return account
    except PendingRollbackError:
        session.rollback()


def update_account(username: str, name: str, category: str, followers_number: int) -> Account:
    account = session.query(Account).filter(Account.username == username).first()
    account.name = name
    account.category = category
    account.followers_number = followers_number
    session.commit()
    return account


def get_account_by_username(username: str) -> Account:
    return session.query(Account).filter(Account.username == username).first()


def add_or_update_account(username: str, name: str, category: str, followers_number: int) -> Account:
    if get_account_by_username(username):
        return update_account(username, name, category, followers_number)
    else:
        return add_account(username, name, category, followers_number)


def list_accounts() -> list:
    return session.query(Account).all()


# Post


def add_post(description: str, posted_time: datetime, author_id: int) -> Union[Post, None]:
    try:
        post = Post(description=description, posted_time=posted_time, author_id=author_id)
        session.add(post)
        session.commit()
        return post
    except IntegrityError:
        return None


# Mention


def add_mention(post_id: int, mentioned_account_id: int) -> Mention:
    mention = Mention(post_id=post_id, mentioned_account_id=mentioned_account_id)
    session.add(mention)
    session.commit()
    return mention


def get_mentioned_accounts_by_author(username: str) -> list:
    try:
        author = session.query(Account).filter(Account.username == username).first()
    except PendingRollbackError:
        session.rollback()
        author = session.query(Account).filter(Account.username == username).first()
    mentioned_accounts = []
    for post in session.query(Post).filter(Post.author_id == author.id).all():
        for mention in session.query(Mention).filter(Mention.post_id == post.id).all():
            mentioned_accounts.append(session.query(Account).filter(Account.id == mention.mentioned_account_id).first())
    return mentioned_accounts
