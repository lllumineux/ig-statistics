from dotenv import load_dotenv, find_dotenv

from src.db.models import Base
from src.initialization import get_engine


def main():
    load_dotenv(find_dotenv())

    engine = get_engine()

    with engine.connect():
        Base.metadata.create_all(engine)


if __name__ == '__main__':
    main()
