from sqlalchemy import Column, String, Integer, DateTime, ForeignKey, UniqueConstraint
from sqlalchemy.orm import declarative_base


Base = declarative_base()


class Account(Base):
    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True)
    username = Column(String(64), unique=True, nullable=False)
    name = Column(String(256))
    category = Column(String(128))
    followers_number = Column(Integer, nullable=False)


class Post(Base):
    __tablename__ = 'posts'
    __table_args__ = (UniqueConstraint('description', 'posted_time', 'author_id', name='_d_pt_aid_uc'),)

    id = Column(Integer, primary_key=True)
    description = Column(String)
    posted_time = Column(DateTime, nullable=False)

    author_id = Column(Integer, ForeignKey('accounts.id'), nullable=False)


class Mention(Base):
    __tablename__ = 'mentions'

    id = Column(Integer, primary_key=True)

    post_id = Column(Integer, ForeignKey('posts.id'), nullable=False)
    mentioned_account_id = Column(Integer, ForeignKey('accounts.id'), nullable=False)
