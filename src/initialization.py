import logging
import os

from dotenv import load_dotenv, find_dotenv
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from webdriver_manager.chrome import ChromeDriverManager


def get_engine():
    load_dotenv(find_dotenv())

    is_debug = os.getenv('IS_DEBUG') == 'True'

    db = os.getenv("POSTGRES_DB")
    user = os.getenv("POSTGRES_USER")
    password = os.getenv("POSTGRES_PASSWORD")

    return create_engine(f'postgresql://{user}:{password}@localhost:5432/{db}', echo=is_debug)


def get_driver():
    load_dotenv(find_dotenv())

    is_debug = os.getenv('IS_DEBUG') == 'True'

    service = Service(ChromeDriverManager(log_level={True: logging.INFO, False: logging.ERROR}[is_debug]).install())
    options = webdriver.ChromeOptions()

    if not is_debug:
        options.add_argument('headless')

    return webdriver.Chrome(service=service, options=options)


session = sessionmaker(bind=get_engine())()
driver = get_driver()
