import click
from dotenv import load_dotenv, find_dotenv

from utils.db_utils import add_or_update_account, add_post, add_mention, get_mentioned_accounts_by_author
from utils.parser_utils import get_account, get_posts, start, end
from utils.utils import get_mentions_from_description, exclude_personal_accounts, generate_graph, extended_print


@click.command()
@click.option(
    '--min-year',
    'min_post_year',
    default=2017,
    help='Posts published earlier than the specified year will not be processed.'
)
@click.option(
    '--min-followers',
    'min_followers_number',
    default=5000,
    help='Accounts with less than the specified number of followers will not be displayed.'
)
@click.option(
    '--update/--no-update',
    'is_update',
    default=True,
    help='Either fetch data or use the saved one.'
)
@click.option(
    '--output-directory',
    'output_directory_name',
    default='stats',
    help='Name of the directory where to store graphs.'
)
@click.argument(
    'accounts',
    nargs=-1
)
def main(
    accounts,
    min_post_year: int = 2017,
    min_followers_number: int = 5000,
    is_update: bool = True,
    output_directory_name: str = 'stats'
) -> None:
    load_dotenv(find_dotenv())

    if len(accounts) < 1:
        extended_print('Error: The number of accounts must be greater than 0', msg_effect='error')
        exit()

    if is_update:
        start()

    for target_account_username in accounts:
        extended_print(f'\nAccount: {target_account_username}', msg_effect='bold')
        if is_update:
            target_account_data = get_account(target_account_username[1:])
            if target_account_data:
                target_account = add_or_update_account(**target_account_data)

                for post_data in get_posts(target_account.username[1:], min_year=min_post_year):
                    post = add_post(**post_data, author_id=target_account.id)

                    if not post:
                        extended_print('Successfully parsed posts', msg_effect='success', remove_prev_line=True)
                        break

                    if post.description:
                        counter = 0
                        for mention in list(set(get_mentions_from_description(post.description))):
                            mention_account_data = get_account(mention[1:])
                            if mention_account_data:
                                mention_account = add_or_update_account(**mention_account_data)
                                add_mention(post_id=post.id, mentioned_account_id=mention_account.id)
                            counter += 1
                        # Crutch for "remove_prev_line=True" in print('Processing posts')
                        if counter > 0:
                            print()

        mentioned_accounts = exclude_personal_accounts(
            get_mentioned_accounts_by_author(target_account_username),
            min_followers_number=min_followers_number
        )
        mentions = [f'{account.name} ({account.username})' for account in mentioned_accounts]
        generate_graph(mentions, target_account_username, output_directory_name=output_directory_name)

    if is_update:
        end()


if __name__ == '__main__':
    # Crutch for google-translator-api "print('Using backend server Russia.')"
    extended_print('', remove_prev_line=True, end='')
    main()
